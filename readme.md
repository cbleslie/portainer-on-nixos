# Portainer on NixOS
> Part of my inevitable realization that it's going to take time for Docker to give up the ghost to NixOS. 


## What is Portainer?
Tough question, in short, it's a Docker Management Platform. Pretty boring answer.

A better description would be it's a remote management WebUI for docker-compose, and docker itself. It does more than this; but that's close enough.

The version we're using here is CE (Community Edition). 

Learn more about [Portainer & Portainer Edge Agent](https://docs.portainer.io/)

## Why would I need this? I'm on NixOS. I'm cool.
You are. 

Sometimes we are forced to deal with conventional docker systems for services or development/testing. Portainer makes for a great subsitiute of the Docker UI on systems that have poor or no support for it.

This NixOS module takes away *a lot* of the updating pain that generic Portainer Deployment methods have. Basically, you change a version number, and rebuild. Done. In my experiance this is expecially helpful for Edge Agent Deploys. Which often involved SSH'ing into servers, stoping docker services, deleteing containers, and restarting them all while managing ID's and Keys that I swear I've never forgotten. It was really cumbersome until I made this for myself.

## Why would I *not* want to use this?
If you don't need to teardown, and build up stacks all the time or have legacy stacks. Maybe you're a declartive girl, you want to live in a delcaritive world. I know I do. 

I would suggest rolling your services into [OCI containers on NixOS](https://nixos.wiki/wiki/NixOS_Containers) for longevity's sake (this is actually how this flake works). 

## So what are my other options?
If that seems not your style, other than using this project, there are projects you could look at for managing docker-compose stacks in more declartive way:

- [Arion](https://docs.hercules-ci.com/arion/) - This is a nix'y wrapper around docker-compose. Pretty cool. Can be run stand-alone, and as a [NixOS Module](https://docs.hercules-ci.com/arion/deployment#_nixos_module)
- [Compose2Nix](https://github.com/aksiksi/compose2nix) - Skips the middleman and turns a docker compose file into a NixOS Config. Pretty wild. 



## Flake Usage for Portainer
This is really setup exclusively for use with flakes.

```nix
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    # Other stuff...
    portainer-on-nixos.url = "gitlab:cbleslie/portainer-on-nixos";
    portainer-on-nixos.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = {
  self,
  nixpkgs,
  # Other stuff...
  portainer-on-nixos,
  ...
  } @inputs: {
    nixosConfigurations."hostname" = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = inputs;
      modules = [
        # Other stuff...
        portainer-on-nixos.nixosModules.portainer # use the module
        # example that shows all default the portainer options
        {
          services.portainer = {
            enable = true; # Default false

            version = "latest"; # Default latest, you can check dockerhub for
                                # other tags.

            openFirewall = "false"; # Default false, set to 'true' if you want
                                    # to be able to access via the port on
                                    # something other than localhost. 

            port = 9443; # Sets the port number in both the firewall and
                         # the docker container port mapping itself.
          };
        }
      ];
    };
  };
}

```
## Flake usage for the Edge Agent
Setting up an edge agent is similar.

```nix
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    # Other stuff...
    portainer-on-nixos.url = "gitlab:cbleslie/portainer-on-nixos";
    portainer-on-nixos.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = {
  self,
  nixpkgs,
  # Other stuff...
  portainer-on-nixos,
  ...
  } @inputs: {
    nixosConfigurations."someOtherHostname" = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = inputs;
      modules = [
        # Other stuff...
        portainer-on-nixos.nixosModules.edge-agent # <--------------- Different!
        # example that shows all default the Edge Agent options, plus some
        # required ones.
        {
          services.portainer-edge-agent = { # <-----------------------Different!
            enable = true; # Default false

            version = "latest"; # Default latest, you can check dockerhub for
                                # other tags. This should match the version of
                                # your main Portainer version.

            id = ""; # REQUIRED! Get this id from the Edge Agent Wizard.
            key = ""; # REQUIRED! Get this key from the Edge Agent Wizard.

            openFirewall = "true"; # Default true, set to 'false' if you have
                                    # some other to access scheme.

            port = 9443; # Sets the port number in both the firewall and
                         # the docker container port mapping itself. Edge Agent
                         # make some assumptions about ports. You shouldn't
                         # touch this unless there is some weird conflict.
          };
        }
      ];
    };
  };
}

```

If you run into issues, let me know. I am always looking for a way to improve this. Feel free to submit PR's.